﻿using System;
using Loogn.OrmLite;

namespace TaskCaller.Models.entity
{
    public class Manager
    {
        /// <summary>
        /// 日志名称
        /// </summary>
        [OrmLiteField(IsPrimaryKey = true, InsertIgnore = true)]
        public long Id { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }
    }
}
