﻿using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TaskCaller.Models;

namespace TaskCaller.Repositories.MySql
{
    public abstract class BaseRepository
    {
        private readonly ConnectionStringsConfig connectionStrings;
        public BaseRepository(IOptions<ConnectionStringsConfig> options)
        {
            connectionStrings = options.Value;
        }

        protected MySqlConnection Open()
        {
            return new MySqlConnection(connectionStrings.MySql);
        }
    }
}
