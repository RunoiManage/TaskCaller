﻿using Loogn.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskCaller.Models.entity;

namespace TaskCaller.Repositories
{
    public interface IDelayTaskRepository
    {
        long Add(DelayTask m);
        long Update(DelayTask m);

        OrmLitePageResult<DelayTask> SearchList(string name, int pageIndex, int pageSize);
        DelayTask SingleById(long id);
        int Delete(long id);
        List<DelayTask> SelectReadyList(DateTime beginTime, DateTime endTime);
        List<DelayTask> SelectByIds(List<long> ids);
        int RetryUpdate(DelayTask task);
    }
}
