﻿using System;
using Loogn.OrmLite;
using Microsoft.Extensions.Options;
using TaskCaller.Models;
using TaskCaller.Models.entity;

namespace TaskCaller.Repositories.SqlServer
{
    public class ManagerRepository : BaseRepository, IManagerRespository
    {

        public ManagerRepository(IOptions<ConnectionStringsConfig> options) : base(options)
        {
        }

        public bool Login(string username, string password)
        {
            using (var db = Open())
            {
                var c = db.CountWhere<Manager>(DictBuilder.Assign("Username", username).Assign("Password", password));
                return c > 0;
            }
        }
    }
}
