#   :point_right: 请使用[HttpQuartz](https://gitee.com/loogn/httpquartz)项目 

# ~~TaskCaller~~

#### 介绍
基于.netcore2.2的一套远程任务调度系统，支持cron表达式的定时任务和延迟任务。

#### 软件架构
基于.netcore mvc，使用宿主服务提供任务调度服务；
使用Cron表达式调用定时任务；
使用时间轮机制调用延迟任务；
支持sqlserver和mysql数据库作为存储；
提供UI界面管理任务项；

#### 安装教程

1. 发布TaskCaller项目到指定文件夹
2. 创建TaskCaller数据库(sqlserver或mysql)，使用wwwroot/sql文件夹中对应的脚本建立表结构
3. 修改appsettings.json配置文件，主要指定server.urls和数据库连接字符串
4. 运行dotnet TaskCaller.dll 【windows中建议使用nssm创建成服务；不能部署到IIS，应用程序池回收会停止宿主服务】
5. 在浏览器中访问server.urls指定的地址，并开始美好的工作


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
